export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
export CFLAGS=/usr/local/include:$CFLAGS

sudo apt-get remove ffmpeg x264 libx264-dev lame libmp3lame-dev

sudo apt-get install git yasm checkinstall


#Installation de good yasm
cd
wget http://www.tortall.net/projects/yasm/releases/yasm-1.2.0.tar.gz
tar xzvf yasm-1.2.0.tar.gz
sudo mv ~/yasm-1.2.0/ /usr/local/src/
cd /usr/local/src/yasm-1.2.0
./configure
make
sudo checkinstall

#Installation de Lame

 sudo apt-get remove ffmpeg x264 libx264-dev lame libmp3lame-dev
 cd
 wget http://sourceforge.net/projects/lame/files/lame/3.99/lame-3.99.5.tar.gz &&  tar -zxvf lame-3.99.5.tar.gz && rm lame-3.99.5.tar.gz
 sudo mv ~/lame-3.99.5/ /usr/local/src/
 cd /usr/local/src/lame-3.99.5
 ./configure
 make
 sudo checkinstall

 
#Installation de ''x264''
cd
git clone git://git.videolan.org/x264.git 
sudo mv ~/x264/ /usr/local/src/
cd /usr/local/src/x264
./configure --enable-shared
make
sudo checkinstall


#Installation de ''libvpx''
cd
git clone http://git.chromium.org/webm/libvpx.git
sudo mv ~/libvpx/ /usr/local/src/
cd /usr/local/src/libvpx
./configure
make
sudo checkinstall

#Installation des dépendances
sudo apt-get install build-essential libvorbis-dev libxvidcore-dev libfaac-dev  libfaad2  libtheora-dev libdirac-dev libvdpau-dev libopenjpeg-dev libopencore-amrwb-dev libopencore-amrnb-dev libgsm1-dev libschroedinger-dev libspeex-dev libdc1394-22-dev libsdl1.2-dev libx11-dev libxfixes-dev 

#Installation de FFmpeg
cd
git clone git://git.videolan.org/ffmpeg.git
sudo mv ~/ffmpeg/ /usr/local/src/
cd /usr/local/src/ffmpeg
./configure --enable-libmp3lame --enable-libxvid --enable-libvorbis --enable-gpl --enable-libfaac --enable-libtheora --enable-zlib --disable-shared --enable-libx264 --enable-libdirac --enable-nonfree --enable-version3 --enable-libschroedinger --enable-avfilter --enable-libspeex --enable-libopenjpeg --enable-libgsm --enable-postproc --enable-pthreads --enable-libopencore-amrnb --enable-libopencore-amrwb --enable-ffplay --enable-pthreads --prefix=/usr/local --enable-x11grab --enable-runtime-cpudetect --enable-bzlib --enable-libdc1394 --enable-libvpx
sudo make clean
make

sudo checkinstall
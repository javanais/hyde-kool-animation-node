

var fs = require('fs');
var app = require('http').createServer(function (req, res) {
    var index= fs.createReadStream("socket_client.html");
    index.pipe(res);
});
var io = require('socket.io').listen(app, { log: true })
 
app.listen(10081);

var sockets=[];

io.sockets.on('connection', function (socket) {
    //console.log ("new client");
    sockets.push (socket);
    socket.lines=[];
    socket.emit("connectionEvent", "connectionsuccess");

    socket.on("disconnectClient",function(state){
        socket.disconnect();
    });

    socket.on("userNameEvent",function(userName){
        console.log(userName);
        socket.userName= userName;
    });
    
    socket.on("sendMessage", function(data){
        console.log(socket.userName+ " sent : "+data);
        sockets.forEach(function(s, i) {
            s.emit("messageFromServer", socket.userName+" :" +data);
        });
    });

    socket.on("startDrawing", function(point) {
        socket.lines=[];
        sockets.forEach(function(s, i) {
            s.emit("startDrawingFromServer", point);
        });
    });

    socket.on("drawLine", function(point) {
        socket.lines.push(point);        
    });

    socket.on("stopDrawing", function() {
        sockets.forEach(function(s, i) {
            s.emit("stopDrawingFromServer", socket.lines);
           
        }); 
    });

    socket.on('disconnect', function (socket) {
        console.log("disconnect");
    });
});
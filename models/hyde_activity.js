// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var hydeActivitySchema = new mongoose.Schema ({
    			ip	: String,
				country       : String,
				region    : String,
				city   : String,
				ll   : String,
				date : Date,
				connections : Number,
				version : String,
				system:String
});

// create the model for users and expose it to our app
module.exports = mongoose.model('HydeActivity', hydeActivitySchema);

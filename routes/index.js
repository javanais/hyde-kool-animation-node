var express = require('express');
var router = express.Router();
var geoip = require('geoip-lite');
var HydeActivity = require("../models/hyde_activity.js")
var downloadableVersion='1.0.7';
var description="Improved playback quality";
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { version: downloadableVersion, description:description });
});

router.get('/version', function(req, res, next) {
    var geo = geoip.lookup(req.headers['x-forwarded-for'] || req.connection.remoteAddress);
    var ip=req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    //console.log("geo" , geo);
    if (geo) { 
    	geo.ip=ip;
		HydeActivity.findOne({ 'ip' :  geo.ip  }, function(err, activity) {
			if(activity){
				activity.city = geo.city;
				activity.region = geo.region;
				activity.ll = geo.ll;
				activity.date = new Date();
				activity.connections = activity.connections+1;
				var version = req.query.version;
				if (version) {
					activity.version =version;
				}
				var system = req.query.system;
				if(system){
					activity.system = system;	
				}

				//activity.version =
				activity.save( function(err) {
					res.render ('version', { version: downloadableVersion, description:description });
				});
			} else {
				geo.connections = 1;
				HydeActivity.create (geo, function(err, hyde_activity) {
	  				res.render( 'version', { version: downloadableVersion, description:description });
	    		});		
			}
		});
	}else{
		res.render('version', { version: downloadableVersion, description:description  });
	}
});

router.get('/listusers', function(req, res, next) {
	HydeActivity.find(function(err, hydeactivities) {
  		res.render("listusers", { hydeactivities: hydeactivities });
 	});
});



module.exports = router;